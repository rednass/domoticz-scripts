-- script_device_ping.lua
-- This script will ping the given devices
-- A dummy sensor will be set to On when a device is found and will be set to Off after a set amount of minutes at which the device is not found
-- You can set the amount of minutes after which the sensor is set to off and also the amount of ping attempts each time this script is executed 

commandArray={}

local success
local ps=0
local arr={}
--                ip address, dummy sensor name, variable name, amount of minutes after which the sensor is set to off, amount of attempts 
table.insert(arr,{'192.168.2.1', 'Ping iPhone', 'ping_iphone', 30, 2})
table.insert(arr,{'192.168.2.2', 'Ping PC', 'ping_pc', 5, 1})
table.insert(arr,{'192.168.2.3', 'Ping iPad', 'ping_ipad', 30, 2})

debug = true

for id = 1, #arr do
    a = arr[id]
    if a[5] > 0 then
        for i = 1, a[5] do
            success = os.execute('ping -c 1 -w 1 '..a[1])
            if success == true then
                ps = i
                break
            end
        end
        att = (ps == 1) and "attempt" or "attempts"
        if success then
            if debug == true then
                print("Ping success "..a[2].." ("..ps.." "..att..")")
            end
            if otherdevices[a[2]] ~= 'On' then
                commandArray[a[2]] = 'On'
            end
            if uservariables[a[3]] ~= 1 then
                commandArray['Variable:'..a[3]]= tostring(1)
            end
        else
            if debug == true then
                print("Ping fail "..a[2].." ("..ps.." "..att..")")
            end
            if otherdevices[a[2]] == 'On' then
                if uservariables[a[3]] == a[4] then
                    commandArray[a[2]]='Off'
                else
                    commandArray['Variable:'..a[3]]= tostring((uservariables[a[3]]) + 1)
                end
            end
        end
    end
end
 
return commandArray