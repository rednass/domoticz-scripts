-- script_device_counters.lua
-- This script will update counters when a device has changed
-- The sensor value is used to execute this script only when the sensor changes too that specific status or use 'nil' to count all status changes

commandArray = {}

local arr = {}
-- sensor name, sensor value on which this function executes (executes on every change with 'nil'), counter name, counter id, value to add
table.insert(arr,{'Doorbell button',nil,'Doorbell',170,1})
table.insert(arr,{'Door sensor','Open','Door opened',174,1})

for id = 1, #arr do
    a = arr[id]
    value = devicechanged[a[1]]
    if ( value and ( a[2]==nil or a[2]==value ) ) then
        val_o = otherdevices_svalues[a[3]]
        if (val_o==nil) then val_o = 0 end
        val_n = val_o+a[5]
        print('Set ' .. a[3] .. ' to ' .. val_n)
        table.insert(commandArray,{['UpdateDevice'] = a[4] .. '|0|' .. tostring(val_n)})
    end
end

return commandArray