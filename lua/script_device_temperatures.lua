-- script_device_temperatures.lua
-- This script will update individual devices for temperature, humidity and pressure when a combined device (for example from Weather Underground or Forecast.io) is updated

commandArray = {}

local Tss = {}
-- sensor name, sensor includes pressure?, temp id, hum id, baro id
-- Set the 2nd argument to true when the combined sensor includes pressure. Hum id and baro id can be set to nil if you don't want to use these values
table.insert(Tss,{'$Temphumbaro',true,21,22,23})
table.insert(Tss,{'$TempLivingroom',false,168,169,null})

for id = 1, #Tss do
    Ts = Tss[id]
    if devicechanged[Ts[1]] then
        if (Ts[2]) then
            Temp, Hum, HumFeelsLike, Press, PressForecast = otherdevices_svalues[Ts[1]]:match("([^;]+);([^;]+);([^;]+);([^;]+);([^;]+)")
            Press = tonumber(Press)
            PressForecast = tonumber(PressForecast)
        else
            Temp, Hum, HumFeelsLike = otherdevices_svalues[Ts[1]]:match("([^;]+);([^;]+);([^;]+)")
        end
        Temp = tonumber(Temp)
    	Hum = tonumber(Hum)
    	HumFeelsLike = tonumber(HumFeelsLike)
    	
    	table.insert(commandArray,{['UpdateDevice'] = Ts[3] .. '|0|' .. Temp})
    	if (Ts[4]) then
    	    table.insert(commandArray,{['UpdateDevice'] = Ts[4] .. '|' .. tostring(Hum) .. '|' .. tostring(HumFeelsLike)})
    	end
    	if (Ts[2] and Ts[5]) then
        	table.insert(commandArray,{['UpdateDevice'] = Ts[5] .. '|0|' .. tostring(Press) .. ';' .. tostring(PressForecast)})
    	end
    end
end

return commandArray