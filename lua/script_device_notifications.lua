-- script_device_notifications.lua
-- This script will send a notification when a device has changed
-- The sensor value is used to execute this script only when the sensor changes too that specific status or use 'nil' to send a notification at all status changes

commandArray = {}

local arr = {}
-- sensor name, sensor value on which this function executes (executes on every change with 'nil'), subject, content
table.insert(arr,{'Doorbell button',nil,'Doorbell','The doorbell rang'})
table.insert(arr,{'Mailbox door',nil,'Mail door','You have mail'})

for id = 1, #arr do
    a = arr[id]
    value = devicechanged[a[1]]
    if ( value and ( a[2]==nil or a[2]==value ) ) then
        commandArray['SendNotification'] = a[3] .. '#' .. a[4]
    end
end

return commandArray